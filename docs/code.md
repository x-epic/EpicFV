
# EpicFV代码结构


目前, EpicFV源码组成如下：

```
EpicFV
├── binary					EpicFV运行所需可执行文件
├── example					EpicFV测试用例
├── src						EpicFV源码
├── util      				EpicFV所需库
├── epic_fv      			EpicFV启动脚本
└── epic_fv_start.cpp  		EpicFV入口程序
```

- binary: binary中包含了EpicFV运行所需可执行文件
- example: 提供了EpicFV的测试用例
- src: 包含EpicFV源码，包括Verilog综合工具和命令注册相关代码
- util: util中包含了EpicFV运行所需的库文件
- epic_fv: epic_fv是EpicFV启动脚本
- epic_fv_start.cpp: epic_fv_start.cpp是EpicFv的代码程序入口
