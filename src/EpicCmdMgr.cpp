//
// Created by xiaofoo.
//

#include "EpicCmdMgr.h"

EpicCmdMgr *EpicCmdMgr::_pinstance{nullptr};
// std::mutex EpicFvCmdMgr::_mutex;
pthread_once_t EpicCmdMgr::_ponce = PTHREAD_ONCE_INIT;

EpicCmdMgr::EpicCmdMgr() {}

EpicCmdMgr::~EpicCmdMgr() {
  for (auto it = cmd_map.begin(); it != cmd_map.end(); ++it) {
    delete it->second;
    it->second = nullptr;
  }
  //  std::unordered_map<std::string, EpicFvCommonCmd *> empty_map;
  //  cmd_map.swap(empty_map);
  cmd_map.clear();
}

void EpicCmdMgr::init() { _pinstance = new EpicCmdMgr(); }

EpicCmdMgr *EpicCmdMgr::getInstance() {
  //  std::lock_guard<std::mutex> lock(_mutex);
  //  if (_pinstance == nullptr) {
  //    _pinstance = new EpicFvCmdMgr();
  //  }
  pthread_once(&_ponce, &EpicCmdMgr::init);
  return _pinstance;
}

std::unordered_map<std::string, EpicCommonCmd *> EpicCmdMgr::getCmdMap()
    const {
  return cmd_map;
}

void EpicCmdMgr::addCmdMap(std::string cmd_name, EpicCommonCmd *common_cmd) {
  cmd_map.insert(std::make_pair(cmd_name, common_cmd));
}
