//
// Created by xiaofoo
//

#ifndef EPICFV_EPICAPP_H
#define EPICFV_EPICAPP_H

#include <pthread.h>

#include <unordered_map>
#include <vector>

#include "TclInterpController.h"
#include "EpicExtendCmd.h"

class EpicApp {
 private:
    static EpicApp *_pinstance;
    //    static std::mutex _mutex;

    static pthread_once_t _ponce;
    static void init();

    EpicApp();

    ~EpicApp();

 public:
    EpicApp(EpicApp &other) = delete;

    void operator=(const EpicApp &) = delete;

    static EpicApp *getInstance();
    void regsiter_cmd();
};

#endif  // EPICFV_EPICAPP_H
